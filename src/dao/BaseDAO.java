package dao;

import entity.BaseRow;
import entity.IEnity;

import java.util.ArrayList;

public abstract class BaseDAO implements IDao {
    Database database=Database.getInstance();
    /**
     * Store record in basedao
     *
     * @param row
     * @return
     */
    @Override
    public int insert(IEnity row) {
        return database.insertTable(row.getClass().getSimpleName(), row);
    }

    /**
     * Update record in table basedao
     *
     * @param row
     * @return
     */
    @Override
    public int update(IEnity row) {
        return database.updateTable(row.getClass().getSimpleName(), row);
    }

    /**
     * Delete record in table basedao
     *
     * @param row
     * @return
     */
    @Override
    public boolean delete(IEnity row) {
        return database.deleteTable(row.getClass().getSimpleName(), row);
    }

    /**
     * Find all basedao
     *
     * @param nameEntity
     * @return
     */
    @Override
    public ArrayList<IEnity> findAll(String nameEntity) {
        return database.selectTable(nameEntity);
    }

    /**
     * Find by id basedao
     *
     * @param nameEntity
     * @param id
     * @return
     */
    @Override
    public IEnity findById(String nameEntity, int id) {
        return database.selectByIdTable(nameEntity, id);
    }
}
