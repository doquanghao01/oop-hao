package dao;

import entity.*;

import java.util.ArrayList;


public class Database {
    private ArrayList<IEnity> productTable = new ArrayList<>();
    private ArrayList<IEnity> categoryTable = new ArrayList<>();
    private ArrayList<IEnity> accessoryTable = new ArrayList<>();

    public static String NAME_PRODUCT = "Product";
    public static String NAME_CATEGORY = "Category";
    public static String NAME_ACCESSOTION = "Accessotion";
    public static Database instants;

    public static Database getInstance() {
        if (instants==null){
            instants=new Database();
        }
        return instants;
    }

    /**
     * Insert table
     *
     * @param nameEntity
     * @param row
     * @return
     */
    public int insertTable(String nameEntity, IEnity row) {
        if (nameEntity.equals(NAME_PRODUCT)) {
            productTable.add(row);
            return 1;
        }
        if (nameEntity.equals(NAME_CATEGORY)) {
            categoryTable.add(row);
            return 1;
        }
        if (nameEntity.equals(NAME_ACCESSOTION)) {
            accessoryTable.add(row);
            return 1;
        }
        return 0;
    }

    /**
     * Select table
     *
     * @param nameEntity
     * @return
     */
    public ArrayList<IEnity> selectTable(String nameEntity) {

        if (nameEntity.equals(NAME_PRODUCT)) {
            return productTable;
        }
        if (nameEntity.equals(NAME_CATEGORY)) {
            return categoryTable;
        }
        if (nameEntity.equals(NAME_ACCESSOTION)) {
            return accessoryTable;
        }
        return new ArrayList<>();
    }

    /**
     * Select by id table
     *
     * @param nameEntity
     * @param id
     * @return
     */
    public IEnity selectByIdTable(String nameEntity, int id) {
        if (nameEntity.equals(NAME_PRODUCT)) {
            for (IEnity itemProduct : productTable) {
                if (itemProduct.getId() == id) {
                    return itemProduct;
                }
            }
        }
        if (nameEntity.equals(NAME_CATEGORY)) {
            for (IEnity itemProduct : categoryTable) {
                if (itemProduct.getId() == id) {
                    return itemProduct;
                }
            }
        }
        if (nameEntity.equals(NAME_ACCESSOTION)) {
            for (IEnity itemProduct : accessoryTable) {
                if (itemProduct.getId() == id) {
                    return itemProduct;
                }
            }
        }
        return null;
    }

    /**
     * Select by name
     *
     * @param nameEntity
     * @param name
     * @return
     */
    public IEnity selectByName(String nameEntity, String name) {
        if (nameEntity.equals(NAME_PRODUCT)) {
            for (IEnity itemProduct : productTable) {
                if (itemProduct.getName().equals(name)) {
                    return itemProduct;
                }
            }
        }
        if (nameEntity.equals(NAME_CATEGORY)) {
            for (IEnity itemProduct : categoryTable) {
                if (itemProduct.getName().equals(name)) {
                    return itemProduct;
                }
            }
        }
        if (nameEntity.equals(NAME_ACCESSOTION)) {
            for (IEnity itemProduct : accessoryTable) {
                if (itemProduct.getName().equals(name)) {
                    return itemProduct;
                }
            }
        }
        return null;
    }

    /**
     * Update table
     *
     * @param name
     * @param row
     * @return
     */
    public int updateTable(String name, Object row) {
        if (name.equals(NAME_PRODUCT)) {
            Product product = (Product) row;
            for (int i = 0; i < productTable.size(); i++) {
                if (productTable.get(i).getId() == product.getId()) {
                    productTable.set(i, product);
                    return 1;
                }
            }
        }
        if (name.equals(NAME_CATEGORY)) {
            Category category = (Category) row;
            for (int i = 0; i < categoryTable.size(); i++) {
                if (categoryTable.get(i).getId() == category.getId()) {
                    categoryTable.set(i, category);
                    return 1;
                }
            }
        }
        if (name.equals(NAME_ACCESSOTION)) {
            Accessory accessory = (Accessory) row;
            for (int i = 0; i < accessoryTable.size(); i++) {
                if (accessoryTable.get(i).getId() == accessory.getId()) {
                    accessoryTable.set(i, accessory);
                    return 1;
                }
            }
        }
        return 0;
    }

    /**
     * Delete table
     *
     * @param name
     * @param row
     * @return
     */
    public boolean deleteTable(String name, IEnity row) {
        if (name.equals(NAME_PRODUCT)) {
            for (int i = 0; i < productTable.size(); i++) {
                if (productTable.get(i).getId() == row.getId()) {
                    productTable.remove(i);
                    return true;
                }
            }
        }
        if (name.equals(NAME_CATEGORY)) {
            for (int i = 0; i < categoryTable.size(); i++) {
                if (categoryTable.get(i).getId() == row.getId()) {
                    categoryTable.remove(i);
                    return true;
                }
            }
        }
        if (name.equals(NAME_ACCESSOTION)) {
            for (int i = 0; i < accessoryTable.size(); i++) {
                if (accessoryTable.get(i).getId() == row.getId()) {
                    accessoryTable.remove(i);
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Truncate table
     *
     * @param name
     */
    public void truncateTable(String name) {
        if (name.equals(NAME_PRODUCT)) {
            productTable.removeAll(productTable);
        }
        if (name.equals(NAME_CATEGORY)) {
            categoryTable.removeAll(categoryTable);
        }
        if (name.equals(NAME_ACCESSOTION)) {
            accessoryTable.removeAll(categoryTable);
        }
    }

    /**
     * Update table by id
     *
     * @param id
     * @param row
     * @return
     */
    public int updateTableById(int id, Object row) {
        Product product = new Product();
        Category category = new Category();
        Accessory accessory = new Accessory();
        if (row.getClass().getName().equals(product.getClass().getName())) {
            for (int i = 0; i < productTable.size(); i++) {
                if (productTable.get(i).getId() == id) {
                    productTable.set(i, (Product) row);
                    return 1;
                }
            }
        }
        if (row.getClass().getName().equals(category.getClass().getName())) {
            for (int i = 0; i < categoryTable.size(); i++) {
                if (categoryTable.get(i).getId() == id) {
                    categoryTable.set(i, (Category) row);
                    return 1;
                }
            }
        }
        if (row.getClass().getName().equals(accessory.getClass().getName())) {
            for (int i = 0; i < accessoryTable.size(); i++) {
                if (accessoryTable.get(i).getId() == id) {
                    accessoryTable.set(i, (Accessory) row);
                    return 1;
                }
            }

        }
        return 0;
    }

}


