package dao;

import entity.BaseRow;
import entity.IEnity;

import java.util.ArrayList;

public interface IDao {
    /**
     * Insert basedao
     *
     * @param row
     * @return
     */
    int insert(IEnity row);

    /**
     * Update basedao
     *
     * @param row
     * @return
     */
    int update(IEnity row);

    /**
     * delete basedao
     *
     * @param row
     * @return
     */
    boolean delete(IEnity row);

    /**
     * Find all basedao
     *
     * @param name
     * @return
     */
    ArrayList<IEnity> findAll(String name);

    /**
     * Find by id base dao
     *
     * @param nameEntity
     * @param id
     * @return
     */
    IEnity findById(String nameEntity, int id);
}
