package dao;

import demo.DatabaseDemo;
import entity.Accessory;
import entity.BaseRow;
import entity.IEnity;
import entity.Product;

import java.util.ArrayList;
import java.util.Scanner;

import static dao.Database.NAME_ACCESSOTION;
import static dao.Database.getInstance;

public class AccessoryDAO extends BaseDAO {
    /**
     * Find by name accessory
     *
     * @param name
     * @return
     */
    public Accessory findByNameAccessory(String name) {
        return (Accessory) getInstance().selectByName(NAME_ACCESSOTION, name);
    }

    /**
     * Seach accessory
     *
     * @param price
     * @return
     */
    public ArrayList<Accessory> seachAccessory(int price) {
        ArrayList<Accessory> productArrayList = new ArrayList<>();
        for (IEnity itemProduct : findAll(NAME_ACCESSOTION)) {
            Accessory accessory = (Accessory) itemProduct;
            if (accessory.getPrice() == price) {
                productArrayList.add(accessory);
            }
        }
        return productArrayList;
    }

}
