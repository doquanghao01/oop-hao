package dao;

import entity.BaseRow;
import entity.IEnity;
import entity.Product;

import java.util.ArrayList;

import static dao.Database.NAME_PRODUCT;
import static dao.Database.getInstance;

public class ProductDAO extends BaseDAO {
    /**
     * Find by name product
     * @param name
     * @return
     */
    public Product findByNameProduct(String name){
        return (Product) getInstance().selectByName(NAME_PRODUCT,name);
    }

    /**
     * Seach product
     * @param price
     * @return
     */
    public ArrayList<Product> seachProduct(double price) {
        ArrayList<Product> productArrayList = new ArrayList<>();
        for (IEnity itemProduct : findAll(NAME_PRODUCT)) {
            Product product = (Product) itemProduct;
            if (product.getPrice() == price) {
                productArrayList.add(product);
            }
        }
        return productArrayList;
    }
}
