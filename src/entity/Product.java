package entity;

public class Product extends BaseRow {
    private double price;
    private int quality;
    private int catedgoryId;

    /**
     * Constructor Product no param
     */
    public Product() {
    }

    /**
     * Constructor Product with param
     *
     * @param id
     * @param name
     * @param price
     * @param quality
     * @param catedgoryId
     */

    public Product(int id, String name, double price, int quality, int catedgoryId) {
        super(id, name);
        this.price = price;
        this.quality = quality;
        this.catedgoryId = catedgoryId;
    }

    /**
     * Get price
     *
     * @return
     */
    public double getPrice() {
        return price;
    }

    /**
     * Set param price
     *
     * @param price
     */
    public void setPrice(double price) {
        this.price = price;
    }

    /**
     * Get quality
     *
     * @return
     */
    public int getQuality() {
        return this.quality;
    }

    /**
     * Set param quality
     *
     * @param quality
     */
    public void setQuality(int quality) {
        this.quality = quality;
    }

    /**
     * Get CatedgoryId
     *
     * @return CatedgoryId
     */
    public int getCatedgoryId() {
        return catedgoryId;
    }

    /**
     * Set param catedgoryIdf
     *
     * @param catedgoryId
     */
    public void setCatedgoryId(int catedgoryId) {
        this.catedgoryId = catedgoryId;
    }

    @Override
    public String toString() {
        return "Product{ ID= " + getId() + ", Name= " + getName() + ", Price=" + price + ", Quality=" + quality + ", CatedgoryId=" + catedgoryId + '}';
    }
}
