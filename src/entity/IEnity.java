package entity;

public interface IEnity {
    /**
     * Get id
     *
     * @return
     */
    int getId();

    /**
     * Set param id
     *
     * @param id
     */
    void setId(int id);

    /**
     * Get name
     *
     * @return
     */
    String getName();

    /**
     * Set param name
     *
     * @param name
     */
    void setName(String name);
}
