package entity;

public class Category extends BaseRow {
    /**
     * Constructor Category no param
     */
    public Category() {
    }

    /**
     * Constructor Category with param
     *
     * @param id
     * @param name
     */
    public Category(int id, String name) {
        super(id, name);
    }


}
