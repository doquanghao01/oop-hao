package entity;

public class Accessory extends BaseRow {

    private double price;
    private int quality;

    /**
     * Constructor Accessory no param
     */
    public Accessory() {
    }

    /**
     * Constructor Accessory with param
     *
     * @param id
     * @param name
     * @param price
     * @param quality
     */
    public Accessory(int id, String name, double price, int quality) {
        super(id, name);
        this.price = price;
        this.quality = quality;
    }

    /**
     * Get price
     *
     * @return
     */
    public double getPrice() {
        return price;
    }

    /**
     * Set param price
     *
     * @param price
     */
    public void setPrice(double price) {
        this.price = price;
    }

    /**
     * Get quality
     *
     * @return
     */
    public int getQuality() {
        return quality;
    }

    /**
     * Set param quality
     *
     * @param quality
     */
    public void setQuality(int quality) {
        this.quality = quality;
    }
}
