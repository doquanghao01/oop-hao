package entity;

public abstract class BaseRow implements IEnity {
    int id;
    String name;

    /**
     * Constructor BaseRow no param
     */
    public BaseRow() {
    }

    /**
     * Constructor BaseRow with param
     *
     * @param id
     * @param name
     */
    public BaseRow(int id, String name) {
        id = id;
        name = name;
    }

    /**
     * Get id
     *
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     * Set param id
     *
     * @param id
     */
    public void setId(int id) {
        id = id;
    }

    /**
     * Get name
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * Set param name
     *
     * @param name
     */
    public void setName(String name) {
        name = name;
    }
}
