package demo;

import dao.AccessoryDAO;
import dao.BaseDAO;
import entity.Accessory;
import entity.IEnity;

import static dao.Database.NAME_ACCESSOTION;

public class AccessoryDaoDemo extends BaseDAO {


    AccessoryDAO accessoryDAO = new AccessoryDAO();

    /**
     * Test store record in table accessory
     *
     * @param accessory
     */
    public void insertAccessoryTest(Accessory accessory) {
        if (insert(accessory) == 1) {
            System.out.println("Thêm thành công");
        } else {
            System.out.println("Thêm thất bại");
        }
    }

    /**
     * Test show all table accessory
     */
    public void findAllAccessoryTest() {
        for (IEnity itemCategory : findAll(NAME_ACCESSOTION)) {
            Accessory accessory = (Accessory) itemCategory;
            System.out.println("Id: " + accessory.getId());
            System.out.println("Name: " + accessory.getName());
            System.out.println("Price: " + accessory.getPrice());
            System.out.println("Quality: " + accessory.getQuality());
        }
    }

    /**
     * Test show table accessory by id
     *
     * @param id
     */
    public void findByIdAccessoryTest(int id) {
        Accessory accessory = (Accessory) findById(NAME_ACCESSOTION, id);
        System.out.println("Id: " + accessory.getId());
        System.out.println("Name: " + accessory.getName());
        System.out.println("Price: " + accessory.getPrice());
        System.out.println("Quality: " + accessory.getQuality());
    }

    /**
     * Test show table accessory by name
     *
     * @param name
     */
    public void findByNameAccessoryTest(String name) {
        Accessory accessory = accessoryDAO.findByNameAccessory(name);
        System.out.println("Id: " + accessory.getId());
        System.out.println("Name: " + accessory.getName());
        System.out.println("Price: " + accessory.getPrice());
        System.out.println("Quality: " + accessory.getQuality());
    }

    /**
     * Test update record in table accessory
     *
     * @param accessory
     */
    public void updateAccessoryTest(Accessory accessory) {
        if (update(accessory) == 1) {
            System.out.println("Sửa thành công");
        } else {
            System.out.println("Sửa thất bại");
        }
    }

    /**
     * Test delete record in table accessory
     * @param accessory
     */
    public void deleteAccessoryTest(Accessory accessory) {
        if (delete(accessory)) {
            System.out.println("Xóa thành công");
        } else {
            System.out.println("Xóa thất bại");
        }
    }
}
