package demo;

import entity.Product;

public class ProductDemo {
    /**
     * Store record in table Product
     * @return product
     */
    public Product createProductTest() {
        Product product = new Product(1, "VGA", 350.0, 10, 1);
        return product;
    }

    /**
     * Show Table Product
     * @param product
     */
    public void printProduct(Product product) {
        System.out.println("id:" + product.getId());
        System.out.println("name:" + product.getName());
        System.out.println("price:" + product.getPrice());
        System.out.println("Quality:" + product.getQuality());
        System.out.println("category:" + product.getCatedgoryId());
    }


}

