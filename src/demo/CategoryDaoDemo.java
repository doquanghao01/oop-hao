package demo;

import dao.BaseDAO;
import dao.CategoryDAO;
import dao.Database;
import entity.BaseRow;
import entity.Category;
import entity.IEnity;

import java.util.ArrayList;

import static dao.Database.NAME_CATEGORY;

public class CategoryDaoDemo extends BaseDAO {
    /**
     * Test store record in table category
     *
     * @param category
     */
    public void insertCategoryTest(Category category) {
        if (insert(category) == 1) {
            System.out.println("Thêm thành công");
        } else {
            System.out.println("Thêm thất bại");
        }
    }

    /**
     * Test show all table category
     */
    public void findAllCategoryTest() {
        for (IEnity itemCategory : findAll(NAME_CATEGORY)) {
            Category category = (Category) itemCategory;
            System.out.println("Id: " + category.getId());
            System.out.println("Name: " + category.getName());
        }
    }

    /**
     * Test show table category by id
     *
     * @param id
     */
    public void findByIdCategoryTest(int id) {
        System.out.println("Id: " + findById(NAME_CATEGORY, id).getId());
        System.out.println("Name: " + findById(NAME_CATEGORY, id).getName());
    }

    /**
     * Test update record in table category
     *
     * @param category
     */
    public void updateCategoryTest(Category category) {
        if (update(category) == 1) {
            System.out.println("Sửa thành công");
        } else {
            System.out.println("Sửa thất bại");
        }
    }

    /**
     * Test delete record in table Product
     *
     * @param category
     */
    public void deleteCategoryTest(Category category) {
        if (delete(category)) {
            System.out.println("Xóa thành công");
        } else {
            System.out.println("Xóa thất bại");
        }
    }

}
