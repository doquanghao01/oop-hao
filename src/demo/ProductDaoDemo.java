package demo;

import dao.BaseDAO;
import dao.ProductDAO;
import entity.BaseRow;
import entity.Category;
import entity.IEnity;
import entity.Product;

import static dao.Database.NAME_PRODUCT;

public class ProductDaoDemo extends BaseDAO {
    ProductDAO productDAO = new ProductDAO();

    /**
     * Test store record in table Product
     *
     * @param product
     */
    public void insertProductTest(Product product) {
        if (insert(product) == 1) {
            System.out.println("Thêm thành công");
        } else {
            System.out.println("Thêm thất bại");
        }
    }

    /**
     * Test show all table Product
     */
    public void findAllProductTest() {
        for (IEnity itemCategory : findAll(NAME_PRODUCT)) {
            Product product = (Product) itemCategory;
            System.out.println(product.toString());
        }
    }

    /**
     * Test show table Product by id
     *
     * @param id
     */
    public void findByIdProductTest(int id) {
        Product product = (Product) findById(NAME_PRODUCT, id);
        System.out.println(product.toString());
    }

    /**
     * Test show table Product by name
     *
     * @param name
     */
    public void findByNameProductTest(String name) {
        Product product = productDAO.findByNameProduct(name);
        System.out.println(product.toString());
    }

    /**
     * Test update record in table Product
     *
     * @param product
     */
    public void updateProductTest(Product product) {
        if (update(product) == 1) {
            System.out.println("Sửa thành công");
        } else {
            System.out.println("Sửa thất bại");
        }
    }

    /**
     * Test delete record in table Product
     *
     * @param product
     */
    public void deleteProductTest(Product product) {
        if (delete(product)) {
            System.out.println("Xóa thành công");
        } else {
            System.out.println("Xóa thất bại");
        }
    }

    public static void main(String[] args) {
        ProductDaoDemo productDaoDemo = new ProductDaoDemo();
        System.out.println("Thêm product:");
        productDaoDemo.insert(new Product(1, "VGA1", 100, 1, 1));
        productDaoDemo.insert(new Product(2, "VGA2", 100, 1, 1));
        productDaoDemo.insert(new Product(3, "VGA3", 100, 1, 1));
        System.out.println("Find All Product:");
        productDaoDemo.findAllProductTest();
        System.out.println("Find by id:");
        productDaoDemo.findByIdProductTest(1);
        System.out.println("Find by name:");
        productDaoDemo.findByNameProductTest("VGA1");
        System.out.println("Update:");
        productDaoDemo.updateProductTest(new Product(1, "VGA4", 99, 10, 1));
        productDaoDemo.findAllProductTest();
        System.out.println("Delete:");
        productDaoDemo.deleteProductTest(new Product(2, "VGA2", 100, 1, 1));
        productDaoDemo.findAllProductTest();
    }
}
