package demo;

import dao.Database;
import entity.*;

import java.util.ArrayList;
import java.util.Scanner;

public class DatabaseDemo extends Database {
    Scanner scanner = new Scanner(System.in);

    /**
     * Test store record in table
     */
    public void insertTableTest() {
        if (insertTable(NAME_PRODUCT, new Product(1, "VGA", 60.0, 35, 3)) == 1) {
            System.out.println("Thêm thành công!");
        }
    }

    /**
     * Test show data table
     */
    public void selectTableTest() {
        for (IEnity itemProduct : selectTable(NAME_PRODUCT)) {
            Product product = (Product) itemProduct;
            System.out.println("id:" + product.getId());
            System.out.println("name:" + product.getName());
            System.out.println("price:" + product.getPrice());
            System.out.println("Quality:" + product.getQuality());
            System.out.println("category:" + product.getCatedgoryId());
        }
    }

    /**
     * Test update record in table
     */
    public void updateTableTest() {
        if (updateTable(NAME_PRODUCT, new Product(5, "VGA1", 400.0, 2, 1)) == 1) {
            System.out.println("Sửa thành công!");
        } else {
            System.out.println("Sửa thất bại!");
        }
    }

    /**
     *  Test delete record in table
     */
    public void deleteTableTest() {
        if (deleteTable(NAME_PRODUCT, new Product(5, "VGA1", 400.0, 2, 1))) {
            System.out.println("Xóa thành công!");
        } else {
            System.out.println("Xóa thất bại!");
        }
    }

    /**
     *  Test truncate record in table
     */
    public void truncateTableTest() {
        truncateTable(NAME_PRODUCT);
    }

    /**
     * Test init record in database
     */
    public void initDatabase() {
        for (int i = 0; i < 10; i++) {
            System.out.println("Nhập id:");
            int Id = scanner.nextInt();
            System.out.println("Nhập Name:");
            String Name = scanner.next();
            System.out.println("Nhập Price:");
            double Price = scanner.nextDouble();
            System.out.println("Nhập Quality:");
            int Quality = scanner.nextInt();
            System.out.println("Nhập CatedgoryId:");
            int CatedgoryId = scanner.nextInt();
            insertTable(NAME_PRODUCT, new Product(Id, Name, Price, Quality, CatedgoryId));
        }
        for (int i = 0; i < 10; i++) {
            System.out.println("Nhập id:");
            int Id = scanner.nextInt();
            System.out.println("Nhập Name:");
            String Name = scanner.next();
            insertTable(NAME_CATEGORY, new Category(Id, Name));
        }
        for (int i = 0; i < 10; i++) {
            System.out.println("Nhập id:");
            int Id = scanner.nextInt();
            System.out.println("Nhập Name:");
            String Name = scanner.next();
            System.out.println("Nhập Price:");
            double Price = scanner.nextDouble();
            System.out.println("Nhập Quality:");
            int Quality = scanner.nextInt();
            insertTable(NAME_ACCESSOTION, new Accessory(Id, Name, Price, Quality));
        }
    }

    /**
     * Test show table
     */
    public void printTableTest() {
        for (IEnity itemProduct : selectTable(NAME_CATEGORY)) {
            Product product = (Product) itemProduct;
            System.out.println("id:" + product.getId());
            System.out.println("name:" + product.getName());
            System.out.println("price:" + product.getPrice());
            System.out.println("Quality:" + product.getQuality());
            System.out.println("category:" + product.getCatedgoryId());
        }
        for (IEnity itemCategory : selectTable(NAME_CATEGORY)) {
            Category category = (Category) itemCategory;
            System.out.println("id:" + category.getId());
            System.out.println("name:" + category.getName());
        }
        for (IEnity itemAccessory : selectTable(NAME_ACCESSOTION)) {
            Accessory accessory = (Accessory) itemAccessory;
            System.out.println("id:" + accessory.getId());
            System.out.println("name:" + accessory.getName());
            System.out.println("price:" + accessory.getPrice());
            System.out.println("Quality:" + accessory.getQuality());
        }
    }

    /**
     * Test update record in table by id
     */
    public void updateTableByIdTest() {
        if (updateTableById(5, new Product(5, "VGA1", 400.0, 2, 1)) == 1) {
            System.out.println("Sửa thành công!");
        } else {
            System.out.println("Sửa thất bại!");
        }
    }

}
